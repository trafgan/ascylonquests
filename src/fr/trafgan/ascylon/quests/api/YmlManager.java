package fr.trafgan.ascylon.quests.api;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import fr.trafgan.ascylon.quests.Quests;


public class YmlManager {
	

	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    return true;
	}
	
	public void createFile(String folder, String file) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void createFolder(String folder) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder);
		if(!files.exists()) {
			files.mkdir();
		}
	}
	
	public boolean fileIsCreate(String folder, String file) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(files.exists()) {
			return true;
		}
		return false;
	}
	
	public boolean folderIsCreate(String folder) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder); 
		if(files.exists()) {
			return true;
		}
		return false;
	}
	
	
	public String getBasicValue(String s) {
		return Quests.getInstance().getConfig().getString(s).replace("&", "§");
	}
	
	public void setValueDouble(String folder, String file, String info, double value) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
			config.set(info, value);
		try {
			config.save(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setValue(String folder, String file, String info, Object value) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
			config.set(info, value);
		try {
			config.save(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setValueInt(String folder, String file, String info, int value) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
			config.set(info, value);
		try {
			config.save(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void setValueString(String folder, String file, String info, String value) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
			config.set(info, value);
		try {
			config.save(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void setValueBoolean(String folder, String file, String info, Boolean value) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
		if(!files.exists()) {
			try {
				files.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
			config.set(info, value);
		try {
			config.save(files);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public ConfigurationSection getList(String folder, String file, String info) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			return null;
		}else {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
			if(config.getConfigurationSection(info) == null) {
				return null;
			}
			return config.getConfigurationSection(info);
		}
	}
	
	public boolean getValueBoolean(String folder, String file, String info) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			return false;
		}else {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
			return config.getBoolean(info);
		}
	}
	
	public double getValueDouble(String folder, String file, String info) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			return 0.00;
		}else {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
			return config.getDouble(info);
		}
	}
	
	public int getValueInteger(String folder, String file, String info) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			return 0;
		}else {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
			return config.getInt(info);
		}
	}
	
	public String getValueString(String folder, String file, String info) {
		File base = Quests.getInstance().getDataFolder();
		File files = new File(base+"/"+folder, file);
		if(!files.exists()) {
			return null;
		}else {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(files);
			return config.getString(info);
		}
	}
}
