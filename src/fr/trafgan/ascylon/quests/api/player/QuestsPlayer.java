package fr.trafgan.ascylon.quests.api.player;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.OfflinePlayer;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.enumeration.QuestType;


public class QuestsPlayer {
	OfflinePlayer player;
	public QuestsPlayer(OfflinePlayer player) {
		this.player = player;
	}
	
	public OfflinePlayer toPlayer() {
		return player;
	}
	
	
	public int getObj(int quest) {
		return Quests.getInstance().yml.getValueInteger("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".obj");
	}
	
	public int getQuestStatus(int quest) {
		return Quests.getInstance().yml.getValueInteger("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".status");
	}
	
	public void setQuestStatus(int quest, int status) {
		Quests.getInstance().yml.setValueInt("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".status", status);
	}
	
	public void setObj(int quest, int number) {
		Quests.getInstance().yml.setValueInt("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".obj", number);
		
	}
	
	public void setTask(int quest, int position) {
		Quests.getInstance().yml.setValueInt("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".task", position);
	}
	
	public List<Integer> getQuests(QuestType questtype){
		List<Integer> quests = new ArrayList<>();
		if(questtype == QuestType.primary) {
			if(Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".primary") != null) {
				for(String quest : Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".primary").getKeys(false)) {
					quests.add(Integer.parseInt(quest));
				}
			}
		}else if(questtype == QuestType.secondary) {
			if(Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".secondary") != null) {
				for(String quest : Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".secondary").getKeys(false)) {
					quests.add(Integer.parseInt(quest));
				}
			}
		}
		return quests;
	}
	public List<Integer> getQuests(){
		List<Integer> quests = new ArrayList<>();
		if(Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".primary") != null) {
			for(String quest : Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".primary").getKeys(false)) {
				quests.add(Integer.parseInt(quest));
			}
		}
		if(Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".secondary") != null) {
			for(String quest : Quests.getInstance().yml.getList("", "players.yml", "players."+player.getName()+".secondary").getKeys(false)) {
				quests.add(Integer.parseInt(quest));
			}
		}
		return quests;
	}
	
	public boolean goToNextStep(int quest) {
		return Quests.getInstance().yml.getValueBoolean("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".nextstep");
	}
	
	public boolean isFinish(int quest) {
		return Quests.getInstance().yml.getValueBoolean("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".finish");
	}
	public void setFinish(int quest, boolean finish) {
		Quests.getInstance().yml.setValueBoolean("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".finish", finish);
	}
	
	public void removeQuest(int quest) {
		Quests.getInstance().yml.setValueString("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest, null);
	}
	
	public void setGoToNextStep(int quest, boolean bool) {
		Quests.getInstance().yml.setValueBoolean("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".nextstep", bool);
	}
	
	public int getTask(int quest) {
		return Quests.getInstance().yml.getValueInteger("", "players.yml", "players."+player.getName()+"."+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString()+"."+quest+".task");
	}
	
	public int getQuest() {
		return Quests.getInstance().yml.getValueInteger("", "players.yml", "players."+player.getName()+".quest");
	}
}
