
package fr.trafgan.ascylon.quests.api.quest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import fr.trafgan.ascylon.quests.enumeration.QuestType;

public class QuestsManager {
	
	
	public List<Integer> getQuests(String name) {
		List<Integer> questslist = new ArrayList<>();
		for(String quest : Quests.getInstance().yml.getList("", "quests.yml", "quests").getKeys(false)) {
			if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".name").contains(name)){
				questslist.add(Integer.parseInt(quest));
			}
		}
		return questslist;
	}
	
	
	public int getStartType(int quest) {
		return Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".start.type");
	}
	public void setStartType(int quest, int starttype) {
		Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.type", starttype);
		if(starttype == 1) { //InZone
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.x", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.y", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.z", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.radius", 1);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args.world", "world");
		}else if(starttype == 2) { //ClickNpc
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args.name", "trafgan");
		}
	}
	public String getStartArg(int quest, String arg) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".start.args."+arg);
	}
	
	public List<Integer> getQuests() {
		List<Integer> questslist = new ArrayList<>();
		for(String quest : Quests.getInstance().yml.getList("", "quests.yml", "quests").getKeys(false)) {
			questslist.add(Integer.parseInt(quest));
		}
		return questslist;
	}
	
	public void changeQuestType(int quest) {
		int type = Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".start.type");
		if(type == 1) {
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.type", 2);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args", null);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args.name", "trafgan");
		}else if(type == 2) {
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.type", 1);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args", null);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.x", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.y", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.z", 1);
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args.radius", 2);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args.world", "trafgan");
		}
	}
	
	public String getName(int quest) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".name");
	}
	
	public int getLastQuest() {
		int quest = 0;
		if(Quests.getInstance().yml.getList("", "quests.yml", "quests") != null) {
			for(String quests : Quests.getInstance().yml.getList("", "quests.yml", "quests").getKeys(false)) {
				quest = Integer.parseInt(quests);
			}
		}
		return quest;
	}
	
	public void setQuestType(int quest, QuestType questtype) {
		Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".type", questtype.getId());
	}
	public int getQuestType(int quest) {
		return Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".type");
	}
	
	public int getLastPosAction(int quest) {
		int pos = 0;
		for(String action : Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".actionsconditions").getKeys(false)) {
			if(Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position") > pos)
			pos = Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position");
		}
		return pos;
	}
	
	public int getLastIdAction(int quest) {
		int id = 0;
		if(Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".actionsconditions") != null) {
			for(String action : Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".actionsconditions").getKeys(false)) {
				 id = Integer.parseInt(action);
			}
		}
		return id;
	}

	public void removeAction(int quest, int id) {
		int actiontoint = 0;
		
		int actuallytarget = 0;
		Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+id, null);
		for(String action : getActions(quest)){
			actiontoint = Integer.parseInt(action);
			
			actuallytarget = Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position");
			if(actiontoint > id) {
				
				Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+actiontoint+".position", actuallytarget-1);
			}
		}
	}
	public void removeCondition(int quest, int id) {
		int actiontoint = 0;
		
		int actuallytarget = 0;
		Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+id, null);
		for(String action : getActions(quest)){
			actiontoint = Integer.parseInt(action);
			
			actuallytarget = Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position");
			if(actiontoint > id) {
				
				Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+actiontoint+".position", actuallytarget-1);
			}
		}
	}
	
	public void setForStart() {
		
	}
	
	public Set<String> getActions(int quest) {
		return Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".actionsconditions").getKeys(false);
	}
	
	public int getPosition(int quest, int action) {
		return Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position");
	}
	
	public String getType(int quest, int id) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+id+".type");
	}
	
	public String getIs(int quest, int id) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+id+".is");
	}
	
	public List<Integer> getTriedAction(int quest) {
		List<Integer> actiontoint = new ArrayList<>();
		for(String argslist : getActions(quest)) {
			actiontoint.add(Integer.parseInt(argslist));
		}
		Arrays.sort(actiontoint.toArray());
		return actiontoint;
	}
	
	public void checkFinish(Player player) {
		
	}
	
	public String getValueActionCondition(int quest, int task, String arg) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+task+".args."+arg);
		
		/*for(String listactions : getActions(quest)) {
			if(Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+listactions+".position") == task) {
				valuereturn =  Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+listactions+".args."+arg);
			}
		}*/
	}
	public void setValueActionCondition(int quest, int task, String arg, String value) {
		Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+task+".args."+arg, value);
	}
	
	public boolean actionHasArg(int quest, int action, String arg) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+action+".args."+arg) != null;
	}
	
	public boolean needConfirm(int quest) {
		return Quests.getInstance().yml.getValueBoolean("", "quests.yml", "quests."+quest+".needconfirm"); 
	}
	public void setNeedConfirm(int quest, boolean need) {
		Quests.getInstance().yml.setValueBoolean("", "quests.yml", "quests."+quest+".needconfirm", need); 
	}
	
	public void changeNeedConfirm(int quest) {
		if(needConfirm(quest)) {
			Quests.getInstance().yml.setValueBoolean("", "quests.yml", "quests."+quest+".needconfirm", false); 
		}else {
			Quests.getInstance().yml.setValueBoolean("", "quests.yml", "quests."+quest+".needconfirm", true); 
		}
	}
	
	public void start(int quest, Player player) {
		QuestsPlayer qp = new QuestsPlayer(player);
		if(!Quests.getInstance().playerquests.containsKey(player) || !Quests.getInstance().playerquests.get(player).contains(quest)) {
			qp.setTask(quest, 1);
			qp.setGoToNextStep(quest, true);
			qp.setFinish(quest, false);
		}
		List<Integer> questlist = qp.getQuests();
		questlist.add(quest);
		if(Quests.getInstance().playerquests.containsKey(player)) {
			Quests.getInstance().playerquests.remove(player);
		}
		Quests.getInstance().playerquests.put(player, questlist);
		new BukkitRunnable() {
			String type = null;
			int task = 1;
			int action = 0;
			@Override
			public void run() {
				if(qp.goToNextStep(quest)) {
					task = qp.getTask(quest);
					qp.setGoToNextStep(quest, false);
					Bukkit.broadcastMessage("quests."+quest+".actionsconditions."+task);
					if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+task) == null) {
						player.sendMessage("�aFin de la qu�te �2"+getName(quest));
						qp.setFinish(quest, true);
						cancel();
						return;
					}
					for(String listactions : Quests.getInstance().qm.getActions(quest)) {
						if(Quests.getInstance().yml.getValueInteger("", "quests.yml", "quests."+quest+".actionsconditions."+listactions+".position") == task) {
							type = Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+listactions+".type");
							action = Integer.parseInt(listactions);
						}
					}
					
					/*for(String questsargs : Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".actions."+task+".args").getKeys(false)) {
						questargs.add(questsargs);
					}
					for(String actionsargs : Quests.getInstance().actions.getArgs(type)) {
						actionargs.add(actionsargs);
					}*/
					
					Class<?> c = null;
					if(Quests.getInstance().yml.getValueString("", "questsaction.yml", "actions."+type+".path") != null || Quests.getInstance().yml.getValueString("", "questscondition.yml", "conditions."+type+".path") != null) {
						try {
							if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+action+".is").equalsIgnoreCase("action")) {
								c = Class.forName(Quests.getInstance().yml.getValueString("", "questsaction.yml", "actions."+type+".path"));
							}else if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+action+".is").equalsIgnoreCase("condition")) {
								c = Class.forName(Quests.getInstance().yml.getValueString("", "questscondition.yml", "conditions."+type+".path"));
							}
							Object t = null;
							t = c.newInstance();
							for(Method method : c.getDeclaredMethods()) {
								String name = method.getName();
								if(name.contains("start")) {
									try {
										@SuppressWarnings("unused")
										Object o = method.invoke(t, quest, action, new QuestsPlayer(player));
									} catch (IllegalAccessException e) {
										e.printStackTrace();
									} catch (IllegalArgumentException e) {
										e.printStackTrace();
									} catch (InvocationTargetException e) {
										e.printStackTrace();
									}
								}
							}
							
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						} catch (InstantiationException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}else {
						player.sendMessage("�aUne quete a une action inn�xistant �2"+type+"�a, Les majuscules sont pri en compte");
						System.out.println("[ERROR - Ascylon Quest] A quest have a non-existent action ("+type+"), capitalization must be taken into account");
					}
					qp.setObj(quest, 0);
					qp.setTask(quest, task+1);
				}
			}
		}.runTaskTimer(Quests.getInstance(), 0, 5);
		
	}
	public int getIdWithPosition(int quest, int position) {
		int id = 0;
		for(String actions : getActions(quest)) {
			int action = Integer.parseInt(actions);
			if(getPosition(quest, action) == position) {
				id = action;
			}
		}
		return id;
	}
	public void changePosition(int quest, int position, int newposition) {
		System.out.println("Position de variable position: "+getIdWithPosition(quest, position));
		int idwithposition = getIdWithPosition(quest, position);
		System.out.println("Position: "+position);
		System.out.println("NewPosition: "+newposition);
		if(newposition <= getLastPosAction(quest)) {
			for(String action : getActions(quest)) {
				int pos1 = getPosition(quest, Integer.parseInt(action));
				System.out.println("Pos1: "+pos1);
				if(position < newposition) {
					if(pos1 > position && pos1 < newposition) {
						System.out.println("Action: "+(Integer.parseInt(action)-1));
						Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position", pos1-1);
					}/*else {
						Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position", pos1-1);
					}*/
				}/*else {
					System.out.println("Pos1: "+pos1+" || ");
					if(pos1 < newposition) {
						System.out.println("Action: "+(Integer.parseInt(action)-1));
						Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+action+".position", pos1-1);
					}
				}*/
				System.out.println();
				Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+idwithposition+".position", newposition);
			}
		}
	}
	public boolean exist(int quest) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest) != null;
	}
	public String getArg(int quest, int id, String arg) {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+id+".args."+arg);
	}
	
	public void addAction(int quest, String action) {
		if(Quests.getInstance().actions.exist(action)) {
			String correctaction = null;
			for(String actions : Quests.getInstance().actions.getActions()) {
				if(actions.equalsIgnoreCase(action)) {
					correctaction = actions;
				}
			}
			int getlastidaction = getLastIdAction(quest)+1;
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".type", correctaction);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".is", "action");
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".position", getLastPosAction(quest)+1);
			for(String args : Quests.getInstance().actions.getArgs(action)) {
				Quests.getInstance().yml.setValue("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".args."+args, Quests.getInstance().yml.getValueString("", "questsaction.yml", "actions."+action+".args."+args));
			}
		}else {
			System.out.println("[ERROR - Ascylon Quest] An action doesn't exists");
		}
	}
	public void addCondition(int quest, String condition) {
		if(Quests.getInstance().conditions.exist(condition)) {
			String correctcondition = null;
			for(String conditions : Quests.getInstance().conditions.getConditions()) {
				if(conditions.equalsIgnoreCase(condition)) {
					correctcondition = conditions;
				}
			}
			int getlastidaction = getLastIdAction(quest)+1;
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".type", correctcondition);
			Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".is", "condition");
			Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".position", getLastPosAction(quest)+1);
			for(String args : Quests.getInstance().conditions.getArgs(condition)) {
				Quests.getInstance().yml.setValue("", "quests.yml", "quests."+quest+".actionsconditions."+getlastidaction+".args."+args, Quests.getInstance().yml.getValueString("", "questscondition.yml", "conditions."+condition+".args."+args));
			}
		}else {
			System.out.println("[ERROR - Ascylon Quest] An action doesn't exists");
		}
	}
	
	public void create(String name) {
		int getlastquest = getLastQuest()+1;
		Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+getlastquest+".name", name);
		setNeedConfirm(getlastquest, true);
		setQuestType(getlastquest, QuestType.secondary);
	}
}
