package fr.trafgan.ascylon.quests.api.condition;

import java.util.Set;

import org.bukkit.OfflinePlayer;

import fr.trafgan.ascylon.quests.Quests;


public class ConditionRegister {
	public void register(ConditionManager conditionmanager) {
		conditionmanager.setArgs();
		Quests.getInstance().yml.setValueString("", "questscondition.yml", "conditions."+conditionmanager.getClass().getSimpleName()+".path", conditionmanager.getClass().toString().replace("class ", ""));
	}
	public void addArg(ConditionManager conditionmanager, String name, Object basicvalue) {
		Quests.getInstance().yml.setValueString("", "questscondition.yml", "conditions."+conditionmanager.getClass().getSimpleName()+".args."+name, basicvalue.toString());
	}
	public Set<String> getConditions(){
		return Quests.getInstance().yml.getList("", "questscondition.yml", "conditions").getKeys(false);
	}
	public Set<String> getArgs(String name) {
		return Quests.getInstance().yml.getList("", "questscondition.yml", "conditions."+name+".args").getKeys(false);
	}
	public void saveId(OfflinePlayer player, int id) {
		if(Quests.getInstance().id != null && Quests.getInstance().id.containsKey(player)) {
			Quests.getInstance().id.remove(player);
		}
		Quests.getInstance().id.put(player, id);
	}
	public int getId(OfflinePlayer player) {
		return Quests.getInstance().id.get(player);
	}
	public boolean exist(String condition) {
		return Quests.getInstance().yml.getValueString("", "questscondition.yml", "conditions."+condition) != null;
	}
}
