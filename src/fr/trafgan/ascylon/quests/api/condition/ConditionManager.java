package fr.trafgan.ascylon.quests.api.condition;


import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public interface ConditionManager {
	public void start(int quest, int id, QuestsPlayer player);
	public void setArgs();
}
