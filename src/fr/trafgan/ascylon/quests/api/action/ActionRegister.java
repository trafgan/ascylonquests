package fr.trafgan.ascylon.quests.api.action;

import java.util.Set;

import fr.trafgan.ascylon.quests.Quests;

public class ActionRegister {
	public void register(ActionManager actionmanager) {
		actionmanager.setArgs();
		Quests.getInstance().yml.setValueString("", "questsaction.yml", "actions."+actionmanager.getClass().getSimpleName()+".path", actionmanager.getClass().toString().replace("class ", ""));
	}
	public Set<String> getActions(){
		return Quests.getInstance().yml.getList("", "questsaction.yml", "actions").getKeys(false);
	}
	public boolean exist(String action) {
		return Quests.getInstance().yml.getValueString("", "questsaction.yml", "actions."+action) != null;
	}
	public void addArg(ActionManager actionmanager, String name, Object basicvalue) {
		Quests.getInstance().yml.setValueString("", "questsaction.yml", "actions."+actionmanager.getClass().getSimpleName()+".args."+name, basicvalue.toString());
	}
	public Set<String> getArgs(String name) {
		return Quests.getInstance().yml.getList("", "questsaction.yml", "actions."+name+".args").getKeys(false);
	}
}
