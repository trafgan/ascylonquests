package fr.trafgan.ascylon.quests.api.action;


import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public interface ActionManager {
	public void start(int quest, int id, QuestsPlayer player);
	public void setArgs();
}
