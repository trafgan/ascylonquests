package fr.trafgan.ascylon.quests.enumeration;

/**
 * @author Trafgan
 *
 */
public enum QuestType {
	primary(1),
	secondary(2);
	
	public int id;
	QuestType(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public static QuestType getType(int id) {
		QuestType questt = QuestType.primary;
		for(QuestType qt : QuestType.values()) {
			if(qt.getId() == id) {
				questt = qt;
			}
		}
		return questt;
	}
}
