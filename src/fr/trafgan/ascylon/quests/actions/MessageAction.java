package fr.trafgan.ascylon.quests.actions;



import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class MessageAction implements ActionManager {

	
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		String message = Quests.getInstance().qm.getArg(quest, id, "message");
		player.toPlayer().getPlayer().sendMessage("");
		player.toPlayer().getPlayer().sendMessage(message.replace("&", "�"));
		player.toPlayer().getPlayer().sendMessage("");
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "message", "salut");
	}

	

}
