package fr.trafgan.ascylon.quests.actions;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class RemovePotionEffect implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		PotionEffectType effect = PotionEffectType.getByName(Quests.getInstance().qm.getArg(quest, id, "effect"));
		player.toPlayer().getPlayer().removePotionEffect(effect);
		player.toPlayer().getPlayer().addPotionEffect(new PotionEffect(effect, 100, 1));
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "effect", PotionEffectType.GLOWING.getName());
	}

}
