package fr.trafgan.ascylon.quests.actions;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class SetHealth implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		int health = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "health"));
		player.toPlayer().getPlayer().setHealth(health);
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "health", 20);
	}

}
