package fr.trafgan.ascylon.quests.actions;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class AddItem implements ActionManager {
	OfflinePlayer player = null;
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		this.player = player.toPlayer();
		player.toPlayer().getPlayer().getInventory().addItem(new ItemStack(Material.getMaterial(Quests.getInstance().qm.getArg(quest, id, "type"))));
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "player", "quest");
		Quests.getInstance().actions.addArg(this, "type", "APPLE");
	}

}
