package fr.trafgan.ascylon.quests.actions;

import org.bukkit.scheduler.BukkitRunnable;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class TimedMessage implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		int time = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "time"));
		String message = Quests.getInstance().qm.getArg(quest, id, "message");
		player.toPlayer().getPlayer().sendMessage("");
		player.toPlayer().getPlayer().sendMessage(message.replace("&", "�"));
		player.toPlayer().getPlayer().sendMessage("");
		new BukkitRunnable() {
			@Override
			public void run() {
				player.setGoToNextStep(quest, true);
			}
		}.runTaskLater(Quests.getInstance(), time);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "time", "20");
		Quests.getInstance().actions.addArg(this, "message", "salut");
	}

}
