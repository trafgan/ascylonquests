package fr.trafgan.ascylon.quests.actions;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.mobs.ActiveMob;
import io.lumine.xikage.mythicmobs.mobs.MobManager;

public class KillMythicMob implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		String name = Quests.getInstance().qm.getArg(quest, id, "name");
		MobManager mm = MythicMobs.inst().getMobManager();
		for(ActiveMob activemob : mm.getActiveMobs()) {
			if(activemob.getType().toString().equalsIgnoreCase(name)) {
				activemob.getEntity().setHealth(0);
			}
		}
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "name", "personalbat");
	}

}
