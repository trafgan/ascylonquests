package fr.trafgan.ascylon.quests.actions;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class AddPotionEffect implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		PotionEffectType effect = PotionEffectType.getByName(Quests.getInstance().qm.getArg(quest, id, "effect"));
		int multiplier = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "multiplier"));
		int time = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "time"));
		player.toPlayer().getPlayer().addPotionEffect(new PotionEffect(effect, time, multiplier));
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "effect", PotionEffectType.GLOWING.getName());
		Quests.getInstance().actions.addArg(this, "multiplier", 1);
		Quests.getInstance().actions.addArg(this, "time", 60);
	}

}
