package fr.trafgan.ascylon.quests.actions;

import org.bukkit.Bukkit;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class BroadcastMessage implements ActionManager {
	String message = null;
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		message = Quests.getInstance().qm.getArg(quest, id, "message");
		Bukkit.broadcastMessage(message);
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "message", "salut");

	}

}
