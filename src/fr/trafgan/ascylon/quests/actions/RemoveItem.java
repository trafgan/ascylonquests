package fr.trafgan.ascylon.quests.actions;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;


public class RemoveItem implements ActionManager {

	// QuestByDark
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		
		Material material = Material.getMaterial(Quests.getInstance().qm.getArg(quest, id, "material"));
		int number = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "number"));
		String name = Quests.getInstance().qm.getArg(quest, id, "name");
		
		ItemStack is = new ItemStack(material, number);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		is.setItemMeta(im);
		
				
		player.toPlayer().getPlayer().getInventory().remove(is);
		
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "material", Material.APPLE.toString());
		Quests.getInstance().actions.addArg(this, "number", 1);
		Quests.getInstance().actions.addArg(this, "name", "Bonjour");

	}

}