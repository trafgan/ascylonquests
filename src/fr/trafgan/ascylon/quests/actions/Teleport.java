package fr.trafgan.ascylon.quests.actions;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class Teleport implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		int x = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "x"));
		int y = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "y"));
		int z = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "z"));
		player.toPlayer().getPlayer().teleport(new Location(Bukkit.getWorld("world"),x,y,z));
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "x", 1);
		Quests.getInstance().actions.addArg(this, "y", 1);
		Quests.getInstance().actions.addArg(this, "z", 1);
	}

}
