package fr.trafgan.ascylon.quests.actions;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.action.ActionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.mobs.ActiveMob;
import io.lumine.xikage.mythicmobs.mobs.MobManager;

public class SpawnMythicMob implements ActionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		int x = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "x"));
		int y = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "y"));
		int z = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "z"));
		int yaw = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "yaw"));
		int pitch = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "pitch"));
		String worldname = Quests.getInstance().qm.getArg(quest, id, "world");
		Location loc = new Location(Bukkit.getWorld(worldname),x,y,z,yaw,pitch);
		MobManager mm = MythicMobs.inst().getMobManager();
		@SuppressWarnings("unused")
		ActiveMob am = mm.spawnMob(Quests.getInstance().qm.getArg(quest, id, "mob"), loc);
		player.setGoToNextStep(quest, true);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().actions.addArg(this, "mob", "personalbat");
		Quests.getInstance().actions.addArg(this, "x", 1);
		Quests.getInstance().actions.addArg(this, "y", 2);
		Quests.getInstance().actions.addArg(this, "z", 3);
		Quests.getInstance().actions.addArg(this, "yaw", 4);
		Quests.getInstance().actions.addArg(this, "pitch", 3);
		Quests.getInstance().actions.addArg(this, "world", "world");
	}

}
