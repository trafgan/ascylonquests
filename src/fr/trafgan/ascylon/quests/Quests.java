package fr.trafgan.ascylon.quests;


import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import fr.trafgan.ascylon.quests.actions.MessageAction;
import fr.trafgan.ascylon.quests.actions.RemoveItem;
import fr.trafgan.ascylon.quests.actions.RemovePotionEffect;
import fr.trafgan.ascylon.quests.actions.SetHealth;
import fr.trafgan.ascylon.quests.actions.SpawnMythicMob;
import fr.trafgan.ascylon.quests.actions.Teleport;
import fr.trafgan.ascylon.quests.actions.TimedMessage;
import fr.trafgan.ascylon.quests.actions.AddItem;
import fr.trafgan.ascylon.quests.actions.AddPotionEffect;
import fr.trafgan.ascylon.quests.actions.BroadcastMessage;
import fr.trafgan.ascylon.quests.actions.KillMythicMob;
import fr.trafgan.ascylon.quests.api.YmlManager;
import fr.trafgan.ascylon.quests.api.action.ActionRegister;
import fr.trafgan.ascylon.quests.api.condition.ConditionRegister;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import fr.trafgan.ascylon.quests.api.quest.QuestsManager;
import fr.trafgan.ascylon.quests.commands.CommandQuests;
import fr.trafgan.ascylon.quests.conditions.GoToLocation;
import fr.trafgan.ascylon.quests.conditions.HasKill;
import fr.trafgan.ascylon.quests.conditions.OnNpcClick;
import fr.trafgan.ascylon.quests.conditions.hasItem;
import fr.trafgan.ascylon.quests.forstart.InZone;
import fr.trafgan.ascylon.quests.forstart.NpcClick;
import fr.trafgan.ascylon.quests.listener.PlaceEvent;

public class Quests extends JavaPlugin {
	private static Quests instance;
	//private String questsyml = "quests.yml";
	//haw
	//g
	
	//G 1 vd
	

	public HashMap<Player, List<Integer>> playerquests = new HashMap<>();
	
	
	// Dark
	public YmlManager yml = new YmlManager();
	
	public static Quests getInstance() {
		return instance;
	}
	
	
	
	
	
	
	
	
	public ActionRegister actions = new ActionRegister();
	
	public ConditionRegister conditions = new ConditionRegister();
	
	public QuestsManager qm = new QuestsManager();
	
	
	
	
	public void registerConditions() {
		conditions.register(new hasItem());
		conditions.register(new HasKill());
		conditions.register(new GoToLocation());
		conditions.register(new OnNpcClick());
		
		
		Bukkit.getPluginManager().registerEvents(new HasKill(), this);
		Bukkit.getPluginManager().registerEvents(new OnNpcClick(), this);
	}
	
	
	public HashMap<OfflinePlayer, Integer> id = new HashMap<>();
	
	public void registerActions() {
		actions.register(new MessageAction());
		actions.register(new AddItem());
		actions.register(new Teleport());
		actions.register(new BroadcastMessage());
		actions.register(new AddPotionEffect());
		actions.register(new RemovePotionEffect());
		actions.register(new SetHealth());
		actions.register(new TimedMessage());
		actions.register(new SpawnMythicMob());
		actions.register(new KillMythicMob());
		actions.register(new RemoveItem());
	}
	
	
	@Override
	public void onEnable() {
		instance = this;
		
		yml.setValueString("", "questsaction.yml", "actions", null);
		yml.setValueString("", "questscondition.yml", "conditions", null);
		registerActions();
		registerConditions();
		Bukkit.getPluginManager().registerEvents(new InZone(), this);
		Bukkit.getPluginManager().registerEvents(new NpcClick(), this);
		for(Player player : Bukkit.getOnlinePlayers()) {
			System.out.println(player.getName());
			QuestsPlayer qp = new QuestsPlayer(player);
			System.out.println("Quest: "+qp.getQuests());
			if(qp.getQuests().size() >= 1) {
				Bukkit.broadcastMessage("Player has quest");
				for(int quest : qp.getQuests()) {
					if(!qp.isFinish(quest)) {
						qp.setTask(quest, qp.getTask(quest)-1);
						qp.setGoToNextStep(quest, true);
						qm.start(quest, player);
					}
				}
				System.out.println("LIST QUEST: "+playerquests.get(player));
			}
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				System.out.println(" ");
				System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
				System.out.println("[Ascylon - Quest] This plugin is private for ascylon");
				System.out.println("[Ascylon - Quest] by Trafgan");
				System.out.println("[Ascylon - Quest] Copyright � - 2018");
				System.out.println(" ");
				System.out.println(" ");
				System.out.println("Actions list:");
				StringBuilder sbactions = new StringBuilder();
				for(String str : actions.getActions()) {
					for(String args : actions.getArgs(str)) {
						sbactions.append(args+" ");
					}
					System.out.println("- "+str+" -> "+sbactions.toString());
					sbactions = new StringBuilder();
				}
				System.out.println(" ");
				System.out.println("Conditions list:");
				StringBuilder sbconditions = new StringBuilder();
				for(String str : conditions.getConditions()) {
					for(String args : conditions.getArgs(str)) {
						sbconditions.append(args+" ");
					}
					System.out.println("- "+str+" -> "+sbconditions.toString());
					sbconditions = new StringBuilder();
				}
				System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
				System.out.println(" ");
				
			}
		}.runTaskLater(this, 20*5);
		
		
		getCommand("quest").setExecutor(new CommandQuests());
		Bukkit.getPluginManager().registerEvents(new PlaceEvent(), this);
	}
	@Override
	public void onDisable() {
		
	}
	
	
}
