package fr.trafgan.ascylon.quests.commands;





import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.enumeration.QuestType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandQuests implements CommandExecutor {

	public boolean isInteger(String nb) {
		try {
			Integer.parseInt(nb);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {
		if(cmd.getName().equalsIgnoreCase("quest")) {
			if(args.length == 0) {
				sender.sendMessage("�cArgument invalide");
				sender.sendMessage("�c-> �6/quest create <name>");
				sender.sendMessage("�c-> �6/quest info <quest>");
				sender.sendMessage("�c-> �6/quest start <id> <player>");
				sender.sendMessage("�c-> �6/quest view <id> <pos1> <pos2>");
				sender.sendMessage("�4�l�n(MARCHE PAS) �c-> �6/quest setposition <id> <position> <newposition>");
				sender.sendMessage("�c-> �6/quest setarg <id> <position> <arg> <value>");
				sender.sendMessage("�c-> �6/quest addaction <id> <action>");
				sender.sendMessage("�c-> �6/quest addcondition <id> <condition>");
				sender.sendMessage("�c-> �6/quest removeaction <id> <position>");
				sender.sendMessage("�c-> �6/quest settype <quest> <type>");
				sender.sendMessage("�c-> �6/quest changeneed <quest>");
				sender.sendMessage("�c-> �6/quest changetype <quest>");
				sender.sendMessage("�c-> �6/quest setargforstart <quest> <arg> <value>");
			}else if(args.length == 1) {
				if(args[0].equalsIgnoreCase("test")) {
					Inventory inv = Bukkit.createInventory(null, 9, "test");
					int slot = 0;
					for(String ges : Quests.getInstance().yml.getList("", "questsaction.yml", "actions").getKeys(false)) {
							
		
						
						ItemStack test = new ItemStack(Material.STONE);
						ItemMeta testmeta = test.getItemMeta();
						testmeta.setDisplayName(ges);
						test.setItemMeta(testmeta);
						inv.setItem(slot, test);
						slot++;
					}
					((Player)sender).openInventory(inv);
				}else if(args[0].equalsIgnoreCase("addaction")) {
					sender.sendMessage("�c-> �6/quest addaction <id> <action>");
				}else if(args[0].equalsIgnoreCase("addcondition")) {
					sender.sendMessage("�c-> �6/quest addcondition <id> <condition>");
				}else if(args[0].equalsIgnoreCase("start")) {
					sender.sendMessage("�c-> �6/quest start <id> <player>");
				}else if(args[0].equalsIgnoreCase("create")) {
					sender.sendMessage("�c-> �6/quest create <name>");
				}else if(args[0].equalsIgnoreCase("view")) {
					sender.sendMessage("�c-> �6/quest view <id> <pos1> <pos2>");
				}else if(args[0].equalsIgnoreCase("setposition")) {
					sender.sendMessage("�c-> �6/quest setposition <id> <position> <newposition>");
				}else if(args[0].equalsIgnoreCase("setarg")) {
					sender.sendMessage("�c-> /quest setarg <id> <position> <arg> <value>");
				}else if(args[0].equalsIgnoreCase("info")) {
					sender.sendMessage("�c-> �6/quest info <quest>");
				}else if(args[0].equalsIgnoreCase("settype")) {
					sender.sendMessage("�c-> �6/quest settype <quest> <type>");
				}else if(args[0].equalsIgnoreCase("changeneed")) {
					sender.sendMessage("�c-> �6/quest changeneed <quest>");
				}else if(args[0].equalsIgnoreCase("setargforstart")) {
					sender.sendMessage("�c-> �6/quest setargforstart <quest> <arg> <value>");
				}else if(args[0].equalsIgnoreCase("changetype")) {
					sender.sendMessage("�c-> �6/quest changetype <quest>");
				}
			}else if(args.length == 2) {
				if(args[0].equalsIgnoreCase("changetype")) {
					int quest = Integer.parseInt(args[1]);
					if(Quests.getInstance().qm.exist(quest)) {
						Quests.getInstance().qm.changeQuestType(quest);
						sender.sendMessage("�aChangement du type du NeedConfirm de la qu�te �2"+Quests.getInstance().qm.getName(quest));
					}else {
						sender.sendMessage("�cLa qu�te n'�xiste pas");
					}
				}else if(args[0].equalsIgnoreCase("changeneed")) {
					int quest = Integer.parseInt(args[1]);
					if(Quests.getInstance().qm.exist(quest)) {
						Quests.getInstance().qm.changeNeedConfirm(quest);
						sender.sendMessage("�aChangement du NeedConfirm en �2"+Quests.getInstance().qm.needConfirm(quest)+" �ade la qu�te �2"+Quests.getInstance().qm.getName(quest));
					}else {
						sender.sendMessage("�cLa qu�te n'�xiste pas");
					}
				}else if(args[0].equalsIgnoreCase("create")) {
					Quests.getInstance().qm.create(args[1]);
					sender.sendMessage("�aQu�te �2"+args[1]+" �acr�e");
				}else if(args[0].equalsIgnoreCase("info")){
					int quest = Integer.parseInt(args[1]);
					if(Quests.getInstance().qm.exist(quest)) {
						sender.sendMessage("");
						sender.sendMessage("�e*-*-*-*-*-*-*-*-*-*-*-*");
						sender.sendMessage("�eNom: �6"+Quests.getInstance().qm.getName(quest));
						if(sender instanceof Player) {
							if(QuestType.getType(Quests.getInstance().qm.getQuestType(quest)) == QuestType.primary) {
								TextComponent tc = new TextComponent("�eType: �6"+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString());
								tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques pour changer en Secondary").create()));
								tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest settype "+quest+" 2"));
								sender.spigot().sendMessage(tc);
							}else if(QuestType.getType(Quests.getInstance().qm.getQuestType(quest)) == QuestType.secondary) {
								TextComponent tc = new TextComponent("�eType: �6"+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)).toString());
								tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques pour changer en Primary").create()));
								tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest settype "+quest+" 1"));
								sender.spigot().sendMessage(tc);
							}
							sender.sendMessage(" ");
							TextComponent tc = new TextComponent("�eCliques ici pour voir/editer les actions et conditions");
							tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques ici").create()));
							tc.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/quest view "+quest+" 1 5"));
							sender.spigot().sendMessage(tc);
							sender.sendMessage(" ");
							tc = new TextComponent("�eCliques ici pour changer le fait de devoir accepter pour commencer la qu�tes");
							sender.sendMessage(" ");
							tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques ici").create()));
							tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest changeneed "+quest));
							sender.spigot().sendMessage(tc);
							sender.sendMessage(" ");
							sender.sendMessage("�eAction pour commencer");
							tc = new TextComponent("�7(Cliques ici pour changer le type)");
							tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques ici").create()));
							tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest changetype "+quest));
							sender.spigot().sendMessage(tc);
							
							for(String arglist : Quests.getInstance().yml.getList("", "quests.yml", "quests."+quest+".start.args").getKeys(false)) {
								tc = new TextComponent("�e- �6"+arglist);
								tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7�l�nCliques ici").create()));
								tc.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/quest setargforstart "+quest+" "+arglist+" valeur"));
								sender.spigot().sendMessage(tc);
							}
						}else {
							sender.sendMessage("�eType: �6"+QuestType.getType(Quests.getInstance().qm.getQuestType(quest)));
						}
						sender.sendMessage("�e*-*-*-*-*-*-*-*-*-*-*-*");
						sender.sendMessage("");
					}else {
						sender.sendMessage("�cLa qu�te n'�xiste pas");
					}
				}else if(args[0].equalsIgnoreCase("addaction")) {
					sender.sendMessage("�c-> �6/quest addaction <id> <action>");
				}else if(args[0].equalsIgnoreCase("addcondition")) {
					sender.sendMessage("�c-> �6/quest addcondition <id> <condition>");
				}else if(args[0].equalsIgnoreCase("start")) {
					sender.sendMessage("�c-> �6/quest start <id> <player>");
				}else if(args[0].equalsIgnoreCase("view")) {
					sender.sendMessage("�c-> �6/quest view <id> <pos1> <pos2>");
				}else if(args[0].equalsIgnoreCase("setposition")) {
					sender.sendMessage("�c-> �6/quest setposition <id> <position> <newposition>");
				}else if(args[0].equalsIgnoreCase("setarg")) {
					sender.sendMessage("�c-> �6/quest setarg <id> <position> <arg> <value>");
				}else if(args[0].equalsIgnoreCase("settype")) {
					sender.sendMessage("�c-> �6/quest settype <quest> <type>");
				}else if(args[0].equalsIgnoreCase("setargforstart")) {
					sender.sendMessage("�c-> �6/quest setargforstart <quest> <arg> <value>");
				}
			}else if(args.length == 3) {
				if(args[0].equalsIgnoreCase("settype")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
								int quest = Integer.parseInt(args[1]);
								int type = Integer.parseInt(args[2]);
								QuestType questtype = QuestType.getType(type);
								Quests.getInstance().qm.setQuestType(quest, questtype);
								sender.sendMessage("�aType de la qu�te �2"+Quests.getInstance().qm.getName(quest)+"("+quest+") �a d�fini en �2"+questtype.toString());
							}else {
								sender.sendMessage("�cLa qu�te n'�xiste pas");
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("addaction")) {
					if(isInteger(args[1])) {
						if(Quests.getInstance().actions.getActions().contains(args[2])) {
							if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
								Quests.getInstance().qm.addAction(Integer.parseInt(args[1]), args[2]);
								sender.sendMessage("�aAction �2"+args[2]+" �aajout� a la quete �2"+Quests.getInstance().qm.getName(Integer.parseInt(args[1])));
							}else {
								sender.sendMessage("�cLa qu�te n'�xiste pas");
							}
						}else {
							sender.sendMessage("�cL'action �4"+args[2]+" �c�xiste pas");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("removeaction")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
								Quests.getInstance().qm.removeAction(Integer.parseInt(args[1]), Quests.getInstance().qm.getIdWithPosition(Integer.parseInt(args[1]), Integer.parseInt(args[2])));
								sender.sendMessage("�aAction a la position �2"+args[2]+" �aretir� a la quete �2"+Quests.getInstance().qm.getName(Integer.parseInt(args[1])));
							}else {
								sender.sendMessage("�cLa qu�te n'�xiste pas");
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("removecondition")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
								Quests.getInstance().qm.removeCondition(Integer.parseInt(args[1]), Quests.getInstance().qm.getIdWithPosition(Integer.parseInt(args[1]), Integer.parseInt(args[2])));
								sender.sendMessage("�aCondition a la position �2"+args[2]+" �aretir� a la quete �2"+Quests.getInstance().qm.getName(Integer.parseInt(args[1])));
							}else {
								sender.sendMessage("�cLa qu�te n'�xiste pas");
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("start")) {
					if(isInteger(args[1])) {
						Player target = Bukkit.getPlayer(args[2]);
						if(target != null) {
							Quests.getInstance().qm.start(Integer.parseInt(args[1]), (Player)sender);
							target.sendMessage("�aD�but de la qu�te �2"+Quests.getInstance().qm.getName(Integer.parseInt(args[1]))+" �a...");
						}else {
							sender.sendMessage("�cCe joueur n'est pas connect�");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("addcondition")) {
					if(isInteger(args[1])) {
						if(Quests.getInstance().conditions.getConditions().contains(args[2])) {
							if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
								Quests.getInstance().qm.addCondition(Integer.parseInt(args[1]), args[2]);
								sender.sendMessage("�aCondition �2"+args[2]+" �aajout� a la quete �2"+Quests.getInstance().qm.getName(Integer.parseInt(args[1])));
							}else {
								sender.sendMessage("�cLa qu�te n'�xiste pas");
							}
						}else {
							sender.sendMessage("�cLa condition �4"+args[2]+" �c�xiste pas");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("view")) {
					sender.sendMessage("�c-> �6/quest view <id> <pos1> <pos2>");
				}else if(args[0].equalsIgnoreCase("setposition")) {
					sender.sendMessage("�c-> �6/quest setposition <id> <position> <newposition>");
				}else if(args[0].equalsIgnoreCase("setarg")) {
					sender.sendMessage("�c-> �6/quest setarg <id> <position> <arg> <value>");
				}else if(args[0].equalsIgnoreCase("setargforstart")) {
					sender.sendMessage("�c-> �6/quest setargforstart <quest> <arg> <value>");
				}
			}else if(args.length == 4) {
				if(args[0].equalsIgnoreCase("view")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							if(isInteger(args[3])) {
								if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
									int quest = Integer.parseInt(args[1]);
									int first = Integer.parseInt(args[2]);
									int second = Integer.parseInt(args[3]);
									sender.sendMessage("");
									for(int x = first ; x <= second ; x++) {
										for(String ids : Quests.getInstance().qm.getActions(quest)) {
											int id = Integer.parseInt(ids);
											if(x == Quests.getInstance().qm.getPosition(quest, id)) {
												String type = Quests.getInstance().qm.getType(quest, id);
												sender.sendMessage("�7("+Quests.getInstance().qm.getPosition(quest, id)+") - �8"+type);
												if(Quests.getInstance().qm.getIs(quest, id).equalsIgnoreCase("action")) {
													for(String actionsargs : Quests.getInstance().actions.getArgs(type)) {
														TextComponent tc = new TextComponent("�7"+actionsargs+" = "+Quests.getInstance().qm.getArg(quest, id, actionsargs));
														tc.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/quest setarg "+args[1]+" "+Quests.getInstance().qm.getPosition(quest, id)+" "+actionsargs+" "+Quests.getInstance().qm.getArg(quest, id, actionsargs)));
														tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7Cliques pour �diter la valeur").create()));
														sender.spigot().sendMessage(tc);
														tc = new TextComponent();
													}
												}else if(Quests.getInstance().qm.getIs(quest, id).equalsIgnoreCase("condition")) {
													for(String conditionsargs : Quests.getInstance().conditions.getArgs(type)) {
														TextComponent tc = new TextComponent("�7"+conditionsargs+" = "+Quests.getInstance().qm.getArg(quest, id, conditionsargs));
														tc.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/quest setarg "+args[1]+" "+Quests.getInstance().qm.getPosition(quest, id)+" "+conditionsargs+" "+Quests.getInstance().qm.getArg(quest, id, conditionsargs)));
														tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7Cliques pour �diter la valeur").create()));
														sender.spigot().sendMessage(tc);
														tc = new TextComponent();
													}
												}
												sender.sendMessage("");
											}
										}
									}
									sender.sendMessage("");
								}else {
									sender.sendMessage("�cLa qu�te �xiste pas");
								}
							}else {
								sender.sendMessage("�cL'argument 4 doit �tre un nombre");	
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("setposition")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							if(isInteger(args[3])) {
								if(Quests.getInstance().qm.exist(Integer.parseInt(args[1]))) {
									Quests.getInstance().qm.changePosition(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
								}else {
									sender.sendMessage("�cLa qu�te �xiste pas");
								}
							}else {
								sender.sendMessage("�cL'argument 4 doit �tre un nombre");	
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}else if(args[0].equalsIgnoreCase("setargforstart")) {
					if(isInteger(args[1])) {
						int quest = Integer.parseInt(args[1]);
						if(Quests.getInstance().qm.exist(quest)) {
							if(isInteger(args[3])) {
								Quests.getInstance().yml.setValueInt("", "quests.yml", "quests."+quest+".start.args."+args[2], Integer.parseInt(args[3]));
							}else {
								Quests.getInstance().yml.setValueString("", "quests.yml", "quests."+quest+".start.args."+args[2], args[3]);
							}
							sender.sendMessage("�aArgument pour commencer �2"+args[2]+" �ade la qu�te �2"+Quests.getInstance().qm.getName(quest)+" �ad�fini a �2"+args[3]);
						}else {
							sender.sendMessage("�cLa qu�te �xiste pas");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}
			}else if(args.length >= 5) {
				if(args[0].equalsIgnoreCase("setarg")) {
					if(isInteger(args[1])) {
						if(isInteger(args[2])) {
							int quest = Integer.parseInt(args[1]);
							if(Quests.getInstance().qm.exist(quest)) {
								int position = Integer.parseInt(args[2]);
								int id = Quests.getInstance().qm.getIdWithPosition(quest, position);
								String arg = args[3];
								if(Quests.getInstance().qm.actionHasArg(quest, id, arg)) {
									StringBuilder sb = new StringBuilder();
									for(int x = 4 ; x < args.length ; x++) {
										if(x == args.length-1) {
											sb.append(args[x]);
										}else {
											sb.append(args[x]+" ");
										}
									}
									Quests.getInstance().qm.setValueActionCondition(quest, id, arg, sb.toString());
									sender.sendMessage("�aArgument �2"+arg+" �ade la qu�te �2"+Quests.getInstance().qm.getName(quest)+" �ad�fini a �2"+sb.toString().replaceAll("&", "�"));
								}else {
									sender.sendMessage("�cL'argument �4"+arg+" �cn'est pas valide pour l");
								}
							}else {
								sender.sendMessage("�cLa qu�tes �4"+quest+" �cn'�xiste pas");
							}
						}else {
							sender.sendMessage("�cL'argument 3 doit �tre un nombre");
						}
					}else {
						sender.sendMessage("�cL'argument 2 doit �tre un nombre");
					}
				}
			}
		}
		return false;
	}
 
}
