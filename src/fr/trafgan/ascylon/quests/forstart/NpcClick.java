package fr.trafgan.ascylon.quests.forstart;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * Author: Trafgan
 * For: Ascylon
 * Creation date: 10 oct. 2018
 */
public class NpcClick implements Listener {
	@EventHandler
	public void onNpcClick(NPCRightClickEvent e) {
		if(Quests.getInstance().qm.getQuests() != null) {
			QuestsPlayer player = new QuestsPlayer(e.getClicker());
			System.out.println("Quest not null");
			System.out.println("PlayerQuests contain player");
			for(Integer quest : Quests.getInstance().qm.getQuests()) {
				if(player.getQuestStatus(quest) == 0) {
					if(Quests.getInstance().qm.getStartType(quest) == 2) {
						if(!player.isFinish(quest)) {
							String npcname = Quests.getInstance().qm.getStartArg(quest, "name");
							if(e.getNPC().getName().equalsIgnoreCase(npcname)) {
								player.setQuestStatus(quest, 1);
								if(Quests.getInstance().qm.needConfirm(quest)) {
									TextComponent tc = new TextComponent("�aCliques pour commenc� la qu�te �2"+Quests.getInstance().qm.getName(quest));
									tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest start "+quest+" "+player.toPlayer().getName()));
									tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7Cliques ici").create()));
									e.getClicker().spigot().sendMessage(tc);
								}else {
									Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/quest start "+quest+" "+player.toPlayer().getName());
									Quests.getInstance().qm.start(quest, player.toPlayer().getPlayer());
								}
							}
						}
						if(!player.isFinish(quest)) {
							
						}
					}
				}
			}
		}
	}
}
