package fr.trafgan.ascylon.quests.forstart;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class InZone implements Listener {
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if((e.getFrom().getBlockX() != e.getTo().getBlockX()) || (e.getFrom().getBlockZ() != e.getFrom().getBlockZ()) || (e.getFrom().getBlockY() != e.getTo().getBlockY())) {
			QuestsPlayer player = new QuestsPlayer(e.getPlayer());
			System.out.println(player.getQuests());
			if(Quests.getInstance().qm.getQuests() != null) {
				System.out.println("Quest not null");
				System.out.println("PlayerQuests contain player");
				for(Integer quest : Quests.getInstance().qm.getQuests()) {
					if(player.getQuestStatus(quest) == 0) {
						if(Quests.getInstance().qm.getStartType(quest) == 1) {
							if(!player.isFinish(quest)) {
								int x = Integer.parseInt(Quests.getInstance().qm.getStartArg(quest, "x"));
								int y = Integer.parseInt(Quests.getInstance().qm.getStartArg(quest, "y"));
								int z = Integer.parseInt(Quests.getInstance().qm.getStartArg(quest, "z"));
								int radius = Integer.parseInt(Quests.getInstance().qm.getStartArg(quest, "radius"));
								World world = Bukkit.getWorld(Quests.getInstance().qm.getStartArg(quest, "world"));
								Location loc = new Location(world,x,y,z);
								System.out.println("Quest ready");
								if(loc.getWorld() == e.getPlayer().getWorld()) {
									System.out.println(loc.distance(player.toPlayer().getPlayer().getLocation()));
									if(loc.distance(player.toPlayer().getPlayer().getLocation()) <= radius) {
										player.setQuestStatus(quest, 1);
										if(Quests.getInstance().qm.needConfirm(quest)) {
											TextComponent tc = new TextComponent("�aCliques pour commenc� la qu�te �2"+Quests.getInstance().qm.getName(quest));
											tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/quest start "+quest+" "+player.toPlayer().getName()));
											tc.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�7Cliques ici").create()));
											e.getPlayer().spigot().sendMessage(tc);
										}else {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/quest start "+quest+" "+player.toPlayer().getName());
											Quests.getInstance().qm.start(quest, player.toPlayer().getPlayer());
										}
										System.out.println("In Zone");
									}
								}
							}
							if(!player.isFinish(quest)) {
								
							}
						}
					}
				}
			}
		}
	}
}
