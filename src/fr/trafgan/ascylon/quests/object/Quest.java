package fr.trafgan.ascylon.quests.object;

import fr.trafgan.ascylon.quests.Quests;

/**
 * Author: Trafgan
 * For: Ascylon
 * Creation date: 10 oct. 2018
 */
public class Quest {
	int quest = 0;
	public Quest() {}
	public Quest(int quest) {
		this.quest = quest;
	}
	public void getQuest(int quest) {
		this.quest = quest;
	}
	
	public boolean exist() {
		return Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest) != null;
	}
}
