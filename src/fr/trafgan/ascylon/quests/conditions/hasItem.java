package fr.trafgan.ascylon.quests.conditions;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.condition.ConditionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class hasItem implements ConditionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		new BukkitRunnable() {
			@Override
			public void run() {
				if(player.toPlayer().getPlayer().getInventory().contains(new ItemStack(Material.getMaterial(Quests.getInstance().qm.getArg(quest, id, "type")), Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "amount"))))) {
					player.setGoToNextStep(quest, true);
					cancel();
				}
			}
		}.runTaskTimer(Quests.getInstance(), 5, 20);
		
	}

	@Override
	public void setArgs() {
		Quests.getInstance().conditions.addArg(this, "type", "APPLE");
		Quests.getInstance().conditions.addArg(this, "amount", 1);
	}


}
