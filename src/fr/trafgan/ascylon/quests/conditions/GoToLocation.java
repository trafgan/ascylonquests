package fr.trafgan.ascylon.quests.conditions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.condition.ConditionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class GoToLocation implements ConditionManager {

	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		int x = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "x"));
		int y = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "y"));
		int z = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "z"));
		int radius = Integer.parseInt(Quests.getInstance().qm.getArg(quest, id, "radius"));
		Location loc = new Location(Bukkit.getWorld(Quests.getInstance().qm.getArg(quest, id, "world")),x,y,z);
		new BukkitRunnable() {
			@Override
			public void run() {
				if(loc.distance(player.toPlayer().getPlayer().getLocation()) <= radius) {
					player.setGoToNextStep(quest, true);
					cancel();
				}
			}
		}.runTaskTimer(Quests.getInstance(), 0, 20);

	}

	@Override
	public void setArgs() {
		Quests.getInstance().conditions.addArg(this, "x", 1);
		Quests.getInstance().conditions.addArg(this, "y", 1);
		Quests.getInstance().conditions.addArg(this, "z", 1);
		Quests.getInstance().conditions.addArg(this, "world", "world");
		Quests.getInstance().conditions.addArg(this, "radius", 2);
	}

}
