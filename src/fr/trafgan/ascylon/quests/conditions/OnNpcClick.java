package fr.trafgan.ascylon.quests.conditions;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.condition.ConditionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;
import net.citizensnpcs.api.event.NPCRightClickEvent;

/**
 * Author: Trafgan
 * For: Ascylon
 * Creation date: 6 oct. 2018
 * ...
 */
public class OnNpcClick implements ConditionManager, Listener {

	//public int id = 0;
	
	@EventHandler
	public void onClick(NPCRightClickEvent e) {
		QuestsPlayer player = new QuestsPlayer(e.getClicker());
		for(int quest : player.getQuests()) {
			if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+(player.getTask(quest)-1)+".type") != null) {
				if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+(player.getTask(quest)-1)+".type").equalsIgnoreCase(this.getClass().getSimpleName())) {
					int id = Quests.getInstance().conditions.getId(player.toPlayer());
					if(e.getNPC().getName().equalsIgnoreCase(Quests.getInstance().qm.getArg(quest, id, "name"))) {
						player.setGoToNextStep(quest, true);
					}
				}
			}
		}
	}
	
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
		Quests.getInstance().conditions.saveId(player.toPlayer(), id);
	}

	@Override
	public void setArgs() {
		Quests.getInstance().conditions.addArg(this, "name", "trafgan");
	}

}
