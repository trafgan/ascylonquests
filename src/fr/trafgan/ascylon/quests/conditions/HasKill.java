package fr.trafgan.ascylon.quests.conditions;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import fr.trafgan.ascylon.quests.Quests;
import fr.trafgan.ascylon.quests.api.condition.ConditionManager;
import fr.trafgan.ascylon.quests.api.player.QuestsPlayer;

public class HasKill implements ConditionManager, Listener {

	QuestsPlayer p = null;
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onKill(EntityDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player) {
			QuestsPlayer qp = new QuestsPlayer((Player)e.getEntity().getKiller());
			for(int quest : qp.getQuests()) {
				if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+(qp.getTask(quest)-1)+".type") != null) {
					if(Quests.getInstance().yml.getValueString("", "quests.yml", "quests."+quest+".actionsconditions."+(qp.getTask(quest)-1)+".type").equalsIgnoreCase(this.getClass().getSimpleName())) {
						int number = Integer.parseInt(Quests.getInstance().qm.getValueActionCondition(quest, qp.getTask(quest)-1, "number"));
						EntityType mob = EntityType.fromName(Quests.getInstance().qm.getValueActionCondition(quest, qp.getTask(quest)-1, "mobtype"));
						String name = Quests.getInstance().qm.getValueActionCondition(quest, qp.getTask(quest)-1, "name");
						if(e.getEntity().getType() == mob) {
							boolean next = false;
							if(!name.equalsIgnoreCase("null")) {
								if(e.getEntity().getName().equalsIgnoreCase(name)) {
									next = true;
								}
							}else {
								next = true;
							}
							if(next == true) {
								qp.setObj(quest, qp.getObj(quest)+1);
							}
						}
						if(qp.getObj(quest) >= number) {
							qp.setGoToNextStep(quest, true);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void start(int quest, int id, QuestsPlayer player) {
	}

	@Override
	public void setArgs() {
		Quests.getInstance().conditions.addArg(this, "name", "null");
		Quests.getInstance().conditions.addArg(this, "number", 5);
		Quests.getInstance().conditions.addArg(this, "mobtype", "WOLF");
	}

}
